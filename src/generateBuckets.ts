
import { groupBy } from "@b08/flat-key";
import { parseTypesInFiles } from "@b08/type-parser";
import { generateBucket } from "./generateBucket";
import { GeneratorOptions, ContentFile } from "./types";

const defaultOptions: GeneratorOptions = {
  quotes: "\"",
  linefeed: "\n",
  aliasMap: {}
};

export function generateBuckets<T extends ContentFile>(files: T[], options: GeneratorOptions = {}): T[] {
  options = {
    ...defaultOptions,
    ...options
  };
  const filtered = files.filter(x => x.extension === ".ts");
  const parsed = parseTypesInFiles(filtered, options);
  const grouped = groupBy(parsed, file => file.file.folder);
  const buckets = grouped.values()
    .map(grp => generateBucket(grp, options))
    .filter(x => x != null);
  return buckets;
}


