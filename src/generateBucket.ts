import { exportedTypes } from "@b08/type-parser";
import { disclaimer } from "./disclaimer/disclaimer";
import { GeneratorOptions, ContentFile, ParsedModel } from "./types";

export function generateBucket<T extends ContentFile>(folder: ParsedModel<T>[], options: GeneratorOptions): T {
  const files = folder.filter(hasExports).map(x => x.file.name);
  if (files.length === 0) { return null; }
  return {
    ...folder[0].file,
    name: "index",
    contents: generateBucketContent(files, options)
  };
}

function hasExports(parsed: ParsedModel): boolean {
  return exportedTypes(parsed).length > 0;
}

function generateBucketContent(files: string[], options: GeneratorOptions): string {
  const exportLines = files.map(file => `export * from ${options.quotes}./${file}${options.quotes};${options.linefeed}`);
  return disclaimer(options) + options.linefeed + exportLines.join("");
}

