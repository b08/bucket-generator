# @b08/bucket-generator, seeded from @b08/generator-seed, library type: generator
This generator creates index.ts files. 

# usage convention
It is recommended to create index file only for your types and interfaces, never for the code. \
For example, I use following naming convention for code. \
user.type.ts - file containing types like "interface User {...}", which describes BL level entity. \
user.model.ts - file containing types like "interface UserModel {...}", which describes REST API level entity. \
user.service.ts - file containing classes like "class UserService". \
...and so on \
And then I generate buckets only for following file types:
type, model, const, enum

# usage

```
import { generateBuckets } from "@b08/bucket-generator";
import { transformRange, transform } from "@b08/gulp-transform";
import * as changed from "gulp-changed";

const options = { lineFeed: "\n", quotes: "\"" };

export function buckets(): NodeJS.ReadWriteStream { // this is a gulp task
  const types = ["type", "model", "const", "enum"];
  const files = types.map(type => `${settings.appPath}/**/*.${type}.ts`);
  return gulp.src(files)
    .pipe(transformRange(files => generateBuckets(files, options)))
    .pipe(changed(dest, { hasChanged: changed.compareContents }))
    .pipe(logWrittenFilesToConsole)
    .pipe(gulp.dest(settings.appPath));
}

const logWrittenFilesToConsole = transform(file => {
  console.log(`Writing ${file.folder}/${file.name}${file.extension}`);
  return file;
});

```

@b08/gulp-transform is a gulp plugin designed to work with generators. \
