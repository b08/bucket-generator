import { test } from "@b08/test-runner";

test("one file bucket", async expect => {
  // arrange
  const srcFile = "../testData/oneFile";
  const target = await import(srcFile);

  // act
  const result = target.func();

  // assert
  expect.equal(result, 1);
});
